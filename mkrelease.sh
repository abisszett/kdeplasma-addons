#!/usr/bin/sh
#set -x
## Create release archive from source:

old_umask="$(umask)"
umask 022

mkdir -p "$(dirname "${0}")"/build
pushd "$(dirname "${0}")"/applets/systemloadviewer/package/ >/dev/null
zip -r ../../../build/org.kde.plasma.systemloadviewer.plasmoid ./*
popd >/dev/null

umask $old_umask
